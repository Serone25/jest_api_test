

export class PostDetailElement extends HTMLElement{
    constructor(){
        super();
        console.log("postdetails cargados");

    }

    cargarPost(post){
        document.querySelector("#title").append(post.title)
        document.querySelector("#body").append(post.content)
    }
    connectedCallback() {
        this.innerHTML = `
                <style>
                    .postsDetail_div {
                        height: 60vh;
                        border: 2px solid black;
                        width: 65%;
                        float:right;
                        margin-top: 80px;
                        margin-right: 80px;
                    }
                    .posts_Detail {
                        font-size: 30px;
                        margin-left: 80px;
                        margin-top: 60px;
                    }
                    .title{
                        padding-right: 60px;
                        margin-left: 180px;
                        font-size: 20px;
                    }
                    .body{
                        padding-right: 54px;
                        margin-left: 180px;
                        font-size: 20px;
                    }
                    .buttons_div{
                        padding-left: 290px;
                        padding-top:40px;
                    }
                    .cancel_button,.update_button,.delete_button {
                        font-size: 16px;
                        margin-right:15px;
                        padding: 12px 50px;
                        background-color:grey;
                        color:white;
                    }
                </style>
                <div class = "postsDetail_div">
                    <h2 class = "posts_Detail">Posts Detail</h2>
                    <form>
                        <span id = "postId"></span>
                        <span class = "title">Title:</span> <textarea id = "title" rows="5" cols="100"></textarea> <br><br>
                        <span class = "body">Body:</span> <textarea id = "body" rows="5" cols="100"></textarea> <br><br>
                    </form>
                    <div class = "buttons_div">
                        <button></button>
                        <button></button>
                        <button></button>
                    </div>
                </div>
            `;
        //this.attachShadow({mode: "open"});          //Establezco el shadow-Dom, por lo que el light no se muestra.
        //this.shadowRoot.innerHTML = "Hola desde el Shadow-DOM";
        
    }
}

customElements.define("postdetail-element", PostDetailElement);