import { PostsRepository } from "../repositories/posts.repository";

///ARRAY EXAMPLE/////

/*export class PutPostsUseCase {
  static async execute() {
    const repository = new PostsRepository();
    const posts = await repository.getAllPosts();
    const newPost ={
        userId: 1,
    id: 2,
    title: "Generacion K",
    body: "Kairos"
    }
    posts.splice(2,1,newPost);

    return posts.map(
      (post) =>
        new Post({
          id: post.id,
          title: post.title,
          content: post.body,
        })
    );
  }
}*/

export class PutPostsUseCase {
  static async execute(posts = [], postModel) {
    const repository = new PostsRepository();
    const postUpdated = await repository.updatePost(postModel);
    const postModelUpdated = {
      id: postUpdated.id,
      title: postUpdated.title,
      content: postUpdated.body,
    };

    return posts.map((post) =>
      post.id === postModelUpdated.id
        ? (post = postModelUpdated)
        : (post = post)
    );
  }
}