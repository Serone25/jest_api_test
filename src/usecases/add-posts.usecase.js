import { Post } from "../model/post";
import { PostsRepository } from "../repositories/posts.repository";

export class AddPostsUseCase {
  static async execute(posts, post) {
    const repository = new PostsRepository();
    const newPostApi =  await repository.addPost(post);
    const newpostModel = new Post({
      id: newPostApi.id,
      title: newPostApi.title,
      content: newPostApi.body,
    });

    return [newpostModel, ...posts];
  }
}




//PUSH ARRAY EXAMPLE
/*export class AddPostsUseCase {
  static async execute() {
    const repository = new PostsRepository();
    const posts = await repository.getAllPosts();
    const myPost ={
        userId: 1,
        id: 101,
        title: "Kairos",
        body: "Hola desde un nuevo post"
    }
    posts.push(myPost);
    return posts.map(
      (post) =>
        new Post({
          id: post.id,
          title: post.title,
          content: post.body,
        })
    );
  }
}*/

//EXAMPLE
/*export class AddPostsUseCase {
  static async execute() {
    const myPost ={
      userId: 1,
      title: "Kairos",
      body: "Hola desde un nuevo post"
    }
    const res = await axios.post("https://jsonplaceholder.typicode.com/posts", myPost);
    return res.data;
  }
}*/