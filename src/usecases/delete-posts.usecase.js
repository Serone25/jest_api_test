import { PostsRepository } from "../repositories/posts.repository";

export class DeletePostsUseCase{
  static async execute(posts=[],postId){
    const repository = new PostsRepository();
    await repository.deletePost(postId);
    return posts.filter((post) => post.id !== postId);
  }
}

//ARRAY EXAMPLE//
/*export class DeletePostsUseCase {
  static async execute() {
    const repository = new PostsRepository();
    const posts = await repository.getAllPosts();
    posts.pop();
    return posts.map(
      (post) =>
        new Post({
          id: post.id,
          title: post.title,
          content: post.body,
        })
    );
  }
}*/

/*export class DeletePostsUseCase {
  static async execute() {
    await axios.delete("https://jsonplaceholder.typicode.com/posts/1");
  return await (
      await axios.get("https://jsonplaceholder.typicode.com/posts")
  ).data;
  }
}*/