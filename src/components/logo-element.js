export class LogoElement extends HTMLElement{
    constructor(){
        super();
        console.log("Logo cargado");
    }

    connectedCallback(){
        this.innerHTML = `
            <style>
                .logo_div {
                    height: 10vh;
                    border: 2px solid black;
                }
                img{
                    height: 60px;
                    width: 200px;
                    padding-left: 10px;
                    padding-top:10px;
                  }
            </style>
            <div class = "logo_div"><img src="./img/kairos.jpeg"></div>
        `;
    }
}

customElements.define("logo-element", LogoElement);