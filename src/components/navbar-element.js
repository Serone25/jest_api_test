export class NavBarElement extends HTMLElement{
    constructor(){
        super();
        console.log("navbar cargada");
    }

    connectedCallback(){
        this.innerHTML = `
            <style>
                .navbar {
                    height: 5vh;
                    border:2px solid black;
                    margin-top:5px;
                }
                .navbar_button {
                    height: 80%;
                    width: 100px;
                    margin-top:5px;
                    margin-left:200px;
                    background-color:white;
                    font-size: 16px;
                }
            </style>
            <div class= "navbar"><button class= "navbar_button">POSTS</button></div>
        `;
    }
}

customElements.define("navbar-element", NavBarElement);