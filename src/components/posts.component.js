import { LitElement, html,css } from "lit";
import { AllPostsUseCase } from "../usecases/all-posts.usecase";
import { DeletePostsUseCase } from "../usecases/delete-posts.usecase";



export class PostsComponent extends LitElement {

    static get styles() {
        return css `
        
        .posts_div {
            margin-top: 40px;
            height: 75vh;
            border: 2px solid black;
            width: 25%;
            float:left;
            overflow-y: scroll;
        }
        .posts_button {
            font-size: 16px;
            padding: 12px 50px;
            float:right;
            background-color:grey;
            color:white;
        }
        .posts_list {
            font-size: 30px;
            margin-left: 20px;
            margin-top: 60px;
        }
        .postsDetail_div {
            height: 60vh;
            border: 2px solid black;
            width: 65%;
            float:right;
            margin-top: 80px;
            margin-right: 80px;
        }
        .posts_Detail {
            font-size: 30px;
            margin-left: 80px;
            margin-top: 60px;
        }
        #title{
            padding-right: 60px;
            margin-left: 180px;
            font-size: 20px;
        }
        #body {
            padding-right: 60px;
            margin-left: 180px;
            font-size: 20px;
            
        }
        
        .buttons_div{
            padding-left: 290px;
            padding-top:40px;
        }
         button {
            font-size: 16px;
            margin-right:15px;
            padding: 12px 50px;
            background-color:grey;
            color:white;
        }
        #update_button {
            display:none;
        }
        #delete_button {
            display:none;
        }
       
    
        
        `;
    }

    static get properties() {
        return {
          posts: { type: Array },
          titleinput: {type: Object},
          contentinput: {type:Object},
          postIdselected: {type: Number},
          post: {type: Object}
        };
      }

    async connectedCallback(){

        super.connectedCallback();
        const posts = [];
        this.posts = await AllPostsUseCase.execute();
        this.myPostsList = this.posts;

        const addButton = this.shadowRoot.getElementById("addbutton");
        const updateButton = this.shadowRoot.getElementById("update_button");
        const deleteButton = this.shadowRoot.getElementById("delete_button");
        const cancelButton = this.shadowRoot.getElementById("cancel_button");
    
        updateButton.style.display = "none";
        deleteButton.style.display = "none";

        this.titleinput = this.shadowRoot.querySelector('#titleArea');
        this.contentinput = this.shadowRoot.querySelector('#bodyArea');


        addButton.addEventListener("click", () => {
            updateButton.style.display === "none" ? updateButton.style.display= "inline" : updateButton.style.display="none";
            deleteButton.style.display === "none" ? deleteButton.style.display= "inline" : deleteButton.style.display="none";
      
        });
        cancelButton.addEventListener("click", ()=> {
            this.titleinput.value="";
            this.contentinput.value="";
        });

        deleteButton.addEventListener("click", () => {
            this.deletePost(this.titleinput.value);
        });
        updateButton.addEventListener("click", () => {
            this.updatePost(this.titleinput.value, this.contentinput.value);
        });

    }
    updateInput(title , content, id) {
        this.titleinput.value = title;
        this.contentinput.value = content;
        this.postIdselected = id;
    };
    async deletePost(title) {
        const postToDelete= this.myPostsList.find(post=> post.title === title);
        
        this.myPostsList = await DeletePostsUseCase.execute(this.myPostsList,postToDelete.id);
        const titledeleted = this.shadowRoot.querySelector('#titleArea');
        const contentdeleted = this.shadowRoot.querySelector('#bodyArea');
        titledeleted.value = "";
        contentdeleted.value = "";
        this.requestUpdate();
    }
    updatePost(title, content) {
    
        const newPost = {id: this.postIdselected, title: title, content:content}
        const index = this.myPostsList.findIndex(post=> post.id === this.postIdselected)
    
        if (index !== -1){
            this.myPostsList[index] = newPost;
        }
    
        const titleupdated = this.shadowRoot.querySelector('#titleArea');
        const contentupdated = this.shadowRoot.querySelector('#bodyArea');
        titleupdated.value = "";
        contentupdated.value = "";
        this.requestUpdate();
    }


    
    render() {
        return html `
        <div class = "posts_div">
            <h2 class = "posts_list">Posts List</h2>
            <button id ="addbutton">Add</button>
            <ul>
                ${this.myPostsList?.map(
                (post) => html`<li @click="${() => 
                            this.updateInput(post.title, post.content, post.id)}">
                    <p>${post.title}</p>
                </li>`
                )}
            </ul>
        </div>
        <div class = "postsDetail_div">
            <h2 class = "posts_Detail">Posts Detail</h2>
            <form>
                <span id = "title">Title:</span> <textarea id = "titleArea" rows="5" cols="100"></textarea> <br><br>
                <span id = "body">Body:</span> <textarea id = "bodyArea" rows="5" cols="100"></textarea> <br><br>
            </form>
            <div class = "buttons_div">
                <button id = "cancel_button">Cancel</button>
                <button id = "update_button">Update</button>
                <button id = "delete_button">Delete</button>
            </div>
        </div>
        `;
    }
    /*createRenderRoot(){
        return this;
    }*/

}

customElements.define('genk-posts', PostsComponent);