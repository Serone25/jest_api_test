import "./components/posts.component";

export class PostsElement extends HTMLElement{
    constructor(){
        super();
        console.log("posts cargados");
         
    }

    connectedCallback(){
        this.innerHTML = `
            <style>
                .posts_div {
                    margin-top: 40px;
                    height: 75vh;
                    border: 2px solid black;
                    width: 25%;
                    float:left;
                    overflow-y: scroll;
                }
                .posts_button {
                    font-size: 16px;
                    padding: 12px 50px;
                    float:right;
                    background-color:grey;
                    color:white;
                }
                .posts_list {
                    font-size: 30px;
                    margin-left: 20px;
                    margin-top: 60px;
                }
            </style>
            <div class = "posts_div">
                <h2 class = "posts_list">Posts List</h2>
                <genk-posts class="genkPosts"></genk-posts>
            </div>
        `;
    }
}

customElements.define("posts-element", PostsElement);