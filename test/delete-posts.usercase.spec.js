import { Post } from "../src/model/post";
import { PostsRepository } from "../src/repositories/posts.repository";
import { DeletePostsUseCase } from "../src/usecases/delete-posts.usecase";
import { POSTS } from "./fixtures/posts";

jest.mock("../src/repositories/posts.repository");

describe("Delete a Post", () => {
  beforeEach(() => {
    PostsRepository.mockClear();
  });

  it("should execute properly", async () => {
    const post = new Post({
      id: 1,
      title: "",
      content:"",
    })
    PostsRepository.mockImplementation(() => {
      return {
        deletePost: () => {
          return {
            id: post.id,
            title: post.title,
            body: post.content,
          };
        },
      };
    });

    const postsDeleted = await DeletePostsUseCase.execute(POSTS,post);
    console.log(post)
    //ARRAY EXAMPLE TEST
    //expect(posts.length).toBe(99);
    expect (postsDeleted.length).toBe(99);
    expect (postsDeleted[0].id).toBe(2);
  });
});