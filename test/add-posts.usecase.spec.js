import { Post } from "../src/model/post";
import { PostsRepository } from "../src/repositories/posts.repository";
import { AddPostsUseCase } from "../src/usecases/add-posts.usecase";
import { POSTS } from "./fixtures/posts";

jest.mock("../src/repositories/posts.repository");

describe("Add a new Post", () => {
  beforeEach(() => {
    PostsRepository.mockClear();
  });

  it("should get all posts", async () => {
    const post = new Post({
      id:101,
      content:"Content added",
      title:"Kairos",
    })
    
    PostsRepository.mockImplementation(() => {
      return {
        addPost: () => {
          return {
            id:post.id,
            title: post.title,
            body: post.content,
            userId: 1,
          };
        },
      };
    });

    const postAdded = await AddPostsUseCase.execute(post);

    //ARRAY EXAMPLE TEST
    //expect(posts.length).toBe(100);

    expect(postAdded.title).toBe(post.title);
    expect(postAdded.id).toBe(post.id);
  });
});