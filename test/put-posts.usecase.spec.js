import { Post } from "../src/model/post";
import { PostsRepository } from "../src/repositories/posts.repository";
import { PutPostsUseCase } from "../src/usecases/put-posts.usecase";
import { POSTS } from "../test/fixtures/posts"

jest.mock("../src/repositories/posts.repository");

describe("Update post use case", () => {
  beforeEach(() => {
    PostsRepository.mockClear();
  });

  it("should executing properly", async () => {
    const post = new Post({
      id: 1,
      content: "Content updated",
      title: "Title updated",
    });

    PostsRepository.mockImplementation(() => {
      return {
        updatePost: () => {
          return {
            id: post.id,
            title: post.title,
            body: post.content,
            userId: 1,
          };
        },
      };
    });

    const postsUpdated = await PutPostsUseCase.execute(POSTS, post);

    expect(postsUpdated.length).toBe(100);
    expect(postsUpdated[0].title).toBe(post.title);
    expect(postsUpdated[0].content).toBe(post.content);
  });
});