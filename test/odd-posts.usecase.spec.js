import { PostsRepository } from "../src/repositories/posts.repository";
import { OddPostsUseCase } from "../src/usecases/odd-posts.usecase";
import { POSTS } from "./fixtures/posts";

jest.mock("../src/repositories/posts.repository");

describe("Odd posts use case", () => {
  beforeEach(() => {
    PostsRepository.mockClear();
  });

  it("should get only the odd posts", async () => {
    PostsRepository.mockImplementation(() => {
      return {
        getAllPosts: () => {
          return POSTS;
        },
      };
    });

    const posts = await OddPostsUseCase.execute();

    expect(posts.length).toBe(50);

    expect(posts[0].id).toBe(1);
    expect(posts[1].id).toBe(3);

  });
});